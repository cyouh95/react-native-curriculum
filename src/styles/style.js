import { StyleSheet, Dimensions } from 'react-native';


const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center'
    },
    border: {
        width: Dimensions.get('window').width * 0.9,
        margin: 10,
        borderWidth: 0.5,
        borderColor: 'gray'
    }
});

export default styles;
