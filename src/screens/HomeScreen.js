import React from 'react';
import { View, FlatList, Button } from 'react-native';
import Entry from '../components/Entry';
import styles from '../styles/style';


class HomeScreen extends React.Component {
    constructor() {
        super();

        this.state = {
            entries: []
        };

        this.toDetails = this.toDetails.bind(this);
        this.handleDelete = this.handleDelete.bind(this);
    }

    toDetails(item) {
        this.props.navigation.navigate('Details', item);
    }

    handleDelete() {
        fetch('https://arcane-everglades-44764.herokuapp.com/delete', {
            method: 'DELETE'
        }).then(response => this.setState({ entries: [] }));
    }

    componentDidMount() {
        fetch('https://arcane-everglades-44764.herokuapp.com/entries')
            .then(response => response.json())
            .then(jsonResponse => this.setState({ entries: jsonResponse }));
    }

    componentWillReceiveProps() {
        this.componentDidMount();
    }

    render() {
        return (
            <View style={styles.container}>
                <FlatList
                    data={this.state.entries}
                    renderItem={({item}) => <Entry item={item} toDetails={this.toDetails} />}
                    keyExtractor={item => item['_id']}
                />
                <Button title='DELETE' onPress={this.handleDelete} color='red' />
            </View>
        )
    }
}

export default HomeScreen;
