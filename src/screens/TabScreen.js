import React from 'react';
import Form from '../components/Form';


class TabScreen extends React.Component {
    constructor() {
        super();

        this.submit = this.submit.bind(this);
    }

    submit(title, post) {
        if (title && post) {
            fetch('https://arcane-everglades-44764.herokuapp.com/post', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                body: 'title=' + title + '&post=' + post
            }).then(response => {
                this.props.navigation.navigate('Feed');
            });
        }
    }

    render() {
        return <Form submit={this.submit} />
    }
}

export default TabScreen;
