import React from 'react';
import { Text, TextInput, View, Button } from 'react-native';
import styles from '../styles/style';


class Form extends React.Component {
    constructor() {
        super();

        this.state = {
            title: '',
            post: ''
        };

        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleSubmit() {
        this.props.submit(this.state.title, this.state.post);
        this.setState({ title: '', post: '' });
    }

    render() {
        return (
            <View style={styles.container}>
                <Text>Title:</Text>
                <TextInput
                    style={[styles.border, {height: 40}]}
                    value={this.state.title}
                    onChangeText={(title) => this.setState({ title: title })}
                />
                <Text>Post:</Text>
                <TextInput
                    style={[styles.border, {height: 200}]}
                    value={this.state.post}
                    onChangeText={(post) => this.setState({ post: post })}
                />
                <Button title='SUBMIT' onPress={this.handleSubmit} />
            </View>
        )
    }
}

export default Form;
