import React from 'react';
import { createStackNavigator, createBottomTabNavigator } from 'react-navigation';
import Ionicons from 'react-native-vector-icons/Ionicons';
import HomeScreen from './src/screens/HomeScreen';
import DetailsScreen from './src/screens/DetailsScreen';
import TabScreen from './src/screens/TabScreen';


const StackScreen = createStackNavigator({
    Home: {
        screen: HomeScreen,
        navigationOptions: {
            title: 'Feed'
        }
    },
    Details: {
        screen: DetailsScreen,
        navigationOptions: ({ navigation }) => ({
            title: navigation.state.params.title,
        })
    }
});

const MainScreenNavigator = createBottomTabNavigator({
    Feed: {
        screen: StackScreen,
        navigationOptions: {
            tabBarIcon: ({ focused, tintColor }) => {
                return <Ionicons name='ios-options-outline' size={25} color={tintColor} />;
            }
        }
    },
    Post: {
        screen: TabScreen,
        navigationOptions: {
            tabBarIcon: ({ focused, tintColor }) => {
                return <Ionicons name='ios-create-outline' size={25} color={tintColor} />;
            }
        }
    }
});

class App extends React.Component {
    render() {
        return (
            <MainScreenNavigator onNavigationStateChange={() => this.setState({})} screenProps={this.state} />
        )
    }
}

export default App;
